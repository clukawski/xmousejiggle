from Xlib import X, display

current = display.Display().screen().root.query_pointer()._data
d = display.Display()
s = d.screen()
root = s.root
root.warp_pointer(current["root_x"]+1,current["root_y"])
d.sync()
root = s.root
root.warp_pointer(current["root_x"]-1,current["root_y"])
d.sync()

