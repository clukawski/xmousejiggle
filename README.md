# THIS JIGGLES THE MOUSE

## REQUIRES python-xlib/python2-xlib or whatever the package is on your respective system 

### PLEASE DON'T ACTUALLLY USE THIS UNLESS YOU NEED IT

Moves the mouse 1 pixel to the right and then 1 pixel to the left.

USAGE: python mousejiggle.py [OR] python2 mousejiggle.py
